<?php
include 'init.php';

$sql = "SELECT 
            p.ID_Pozice,
            p.nazev      
        FROM pozice p";

$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$pozice = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $pozice[] = $row;
    }
}

echo json_encode($pozice);

$conn->close();
