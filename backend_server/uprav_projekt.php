<?php
include 'init.php';

function sendErrorResponse($message) {
    echo json_encode(array("success" => false, "message" => $message));
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ID = $_POST['updatedID'] ?? '';
    $Nazev = $_POST['updatedNazev'] ?? '';
    $Popis = $_POST['updatedPopis'] ?? '';
    $Od = $_POST['updatedOd'] ?? '';
    $Do = $_POST['updatedDo'] ?? '';

    if ($Nazev && $Popis && $Od && $Do) {
        $sqlUpdateProject = "UPDATE projekty SET nazev = ?, popis = ?, od = ?, do = ? WHERE ID_projekty = ?" ;
        $stmtProject = $conn->prepare($sqlUpdateProject);

        if ($stmtProject) {
            $stmtProject->bind_param("sssss", $Nazev, $Popis, $Od, $Do, $ID);

            if (!$stmtProject->execute()) {
                echo json_encode(array("success" => false, "message" => "Error updating project: " . $stmtProject->error . $sqlUpdateProject));
                $stmtProject->close();
                $conn->close();
                exit;
            }

            $stmtProject->close();
            echo json_encode(array("success" => true, "message" => "Project updated successfully"));
        } else {
            echo json_encode(array("success" => false, "message" => "Error preparing statement for project: " . $conn->error));
        }
    } else {
        echo json_encode(array("success" => false, "message" => "Required fields for updating are missing"));
    }
} else {
    echo json_encode(array("success" => false, "message" => "Invalid request method"));
}


$conn->close();
?>
