<?php
include 'init.php';

function sendErrorResponse($message) {
    echo json_encode(array("success" => false, "message" => $message));
    exit;
}

$nazev = $_POST['nazev'] ?? '';
$popis = $_POST['popis'] ?? '';
$od = $_POST['od'] ?? '';
$do = $_POST['do'] ?? '';


if (!$nazev || !$popis || !$od || !$do) {
    echo json_encode(array("success" => false, "message" => "Required fields are missing"));
    exit;
}

$sqlPacienti = "INSERT INTO projekty (nazev, popis, od, do) 
                VALUES ('$nazev', '$popis', '$od', '$do')";
if ($conn->query($sqlPacienti) === TRUE) {
    echo json_encode(array("success" => true, "message" => "New project record created successfully"));
} else {
    echo json_encode(array("success" => false, "message" => "Error: " . $conn->error));
}

$conn->close();