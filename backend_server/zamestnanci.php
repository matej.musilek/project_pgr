<?php
include 'init.php';

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type');
    exit;
}

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$sql = "SELECT 
            z.ID_zamestnanec,
            z.jmeno,
            z.prijmeni,
            p.nazev AS pozice,
            a.mesto,
            z.email,
            z.telefon
        FROM zamestnanci z
        INNER JOIN pozice p ON z.ID_pozice = p.ID_pozice
        INNER JOIN adresa a ON z.ID_adresa = a.ID_adresa";

$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$employees = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $employees[] = $row;
    }
}

echo json_encode($employees);

$conn->close();
