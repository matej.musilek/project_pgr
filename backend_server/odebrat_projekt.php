<?php
include 'init.php';


if (isset($_POST['projectID'])) {
    $projectID = $_POST['projectID'];


    $sqlUpdateUkoly = "UPDATE ukoly SET ID_projekty = NULL WHERE ID_projekty = $projectID";


    if ($conn->query($sqlUpdateUkoly) === TRUE) {
        $response = array('success' => true, 'message' => 'Foreign key column emptied successfully');
    } else {
        $response = array('success' => false, 'message' => 'Error updating foreign key column: ' . $conn->error);
    }
} else {
    $response = array('success' => false, 'message' => 'Missing project ID parameter');
}



if (isset($_POST['projectID'])) {
    $projectID = $_POST['projectID'];

 
    $sql = "DELETE FROM projekty WHERE ID_projekty = $projectID";

    if ($conn->query($sql) === TRUE) {
        $response = array('success' => true, 'message' => 'Data deleted successfully');
    } else {
        $response = array('success' => false, 'message' => 'Error deleting data: ' . $conn->error);
    }
} else {
    $response = array('success' => false, 'message' => 'Missing project ID parameter');
}


$conn->close();


header('Content-Type: application/json');
echo json_encode($response);

?>