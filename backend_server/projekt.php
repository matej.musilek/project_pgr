<?php
include 'init.php';

$sql = "SELECT 
            p.ID_projekty,
            p.nazev,
            p.popis,
            p.od,
            p.do    
        FROM projekty p";


$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$project = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $project[] = $row;
    }
}

echo json_encode($project);

$conn->close();
