<?php
include 'init.php';

$sql = "SELECT 
            u.ID_ukoly,
            u.nazev,
            u.termin,
            u.popis,
            s.ID_status,
            s.stav AS status,
            COALESCE(p.nazev, 'Není přiřazeno') AS projekty,
            z.jmeno AS jmeno_zamestnance,
            z.prijmeni AS prijmeni_zamestnance
            FROM ukoly u
            INNER JOIN status s ON u.ID_status = s.ID_status
            LEFT JOIN projekty p ON u.ID_projekty = p.ID_projekty
            LEFT JOIN prirazeni pr ON u.ID_ukoly = pr.ID_ukoly
            LEFT JOIN zamestnanci z ON pr.ID_zamestnanec = z.ID_zamestnanec";
$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$tasks = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $tasks[] = $row;
    }
}

echo json_encode($tasks);

$conn->close();
