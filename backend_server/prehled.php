<?php
include 'init.php';

$sql = "SELECT 
            u.ID_ukoly,
            u.nazev,
            u.termin,
            u.popis,
            s.ID_status,
            s.stav AS status,
            p.nazev AS projekt,
            z.jmeno AS jmeno_zamestnance,
            z.prijmeni AS prijmeni_zamestnance
        FROM ukoly u
        INNER JOIN status s ON u.ID_status = s.ID_status
        INNER JOIN projekty p ON u.ID_projekty = p.ID_projekty
        INNER JOIN prirazeni pr ON u.ID_ukoly = pr.ID_ukoly
        INNER JOIN zamestnanci z ON pr.ID_zamestnanec = z.ID_zamestnanec";

$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$overview = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $overview[] = $row;
    }
}

echo json_encode($overview);

$conn->close();
?>
