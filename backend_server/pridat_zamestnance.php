<?php
include 'init.php';

function sendErrorResponse($message)
{
    echo json_encode(array("success" => false, "message" => $message));
    exit;
}

$jmenoPrijmeni = $_POST['jmenoprijmeni'] ?? '';
$nazevPozice = $_POST['pozice'] ?? '';
$ulice = $_POST['ulice'] ?? '';
$cp = $_POST['cp'] ?? '';
$PSC = $_POST['PSC'] ?? '';
$mesto = $_POST['mesto'] ?? '';
$email = $_POST['email'] ?? '';
$telefon = $_POST['telefon'] ?? '';

if (!$jmenoPrijmeni || !$nazevPozice || !$ulice || !$cp || !$PSC || !$mesto || !$email || !$telefon) {
    echo json_encode(array("success" => false, "message" => "Required fields are missing"));
    exit;
}

// Rozdělení jména a příjmení
list($jmeno, $prijmeni) = explode(' ', $jmenoPrijmeni, 2);

// Získání ID pozice na základě názvu
$sqlPozice = "SELECT ID_pozice FROM pozice WHERE nazev = '$nazevPozice'";
$resultPozice = $conn->query($sqlPozice);

if ($resultPozice->num_rows == 0) {
    sendErrorResponse("Position with the given name not found");
}

$rowPozice = $resultPozice->fetch_assoc();
$poziceID = $rowPozice['ID_pozice'];

// Přidání adresy do tabulky adresa
$sqlAdresa = "INSERT INTO adresa (ulice, cp, PSC, mesto) 
              VALUES ('$ulice', '$cp', '$PSC', '$mesto')";
if ($conn->query($sqlAdresa) !== TRUE) {
    sendErrorResponse("Error adding address: " . $conn->error);
}

// Získání ID adresy
$adresaID = $conn->insert_id;

// Přidání zaměstnance do tabulky zamestnanci
$sqlZamestnanec = "INSERT INTO zamestnanci (jmeno, prijmeni, ID_pozice, ID_adresa, email, telefon) 
                   VALUES ('$jmeno', '$prijmeni', '$poziceID', '$adresaID', '$email', '$telefon')";
if ($conn->query($sqlZamestnanec) !== TRUE) {
    sendErrorResponse("Error adding employee: " . $conn->error);
}

echo json_encode(array("success" => true, "message" => "Data successfully added to the database"));

$conn->close();
