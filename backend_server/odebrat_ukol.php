<?php
include 'init.php';

$response = array('success' => false, 'message' => 'Unknown error');

if (isset($_POST['ukolID'])) {
    $ukolID = $_POST['ukolID'];

    // Smazání záznamů z tabulky prirazeni
    $sqlDeletePrirazeni = "DELETE FROM prirazeni WHERE ID_ukoly = $ukolID";
    if ($conn->query($sqlDeletePrirazeni) === TRUE) {
        // Smazání záznamu z tabulky ukoly
        $sqlDeleteUkoly = "DELETE FROM ukoly WHERE ID_ukoly = $ukolID";
        if ($conn->query($sqlDeleteUkoly) === TRUE) {
            $response = array('success' => true, 'message' => 'Ukol deleted successfully');
        } else {
            $response = array('success' => false, 'message' => 'Error deleting ukol: ' . $conn->error);
        }
    } else {
        $response = array('success' => false, 'message' => 'Error deleting dependent records: ' . $conn->error);
    }
} else {
    $response = array('success' => false, 'message' => 'Missing ukol ID parameter');
}

$conn->close();

header('Content-Type: application/json');
echo json_encode($response);
