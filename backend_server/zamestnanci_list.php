<?php
include 'init.php';

$sql = "SELECT 
            z.ID_zamestnanec,
            z.jmeno,
            z.prijmeni      
        FROM zamestnanci z";

$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$zamestnanec = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $zamestnanec[] = $row;
    }
}

echo json_encode($zamestnanec);

$conn->close();
