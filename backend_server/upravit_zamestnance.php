<?php
include 'init.php';

function sendErrorResponse($message)
{
    echo json_encode(array("success" => false, "message" => $message));
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ID = $_POST['updatedID_zamestnanec'] ?? '';
    $Jmenoprijmeni = $_POST['updatedJmenoprijmeni'] ?? '';
    $Pozice = $_POST['updatedPozice'] ?? '';
    $Mesto = $_POST['updatedMesto'] ?? '';
    $Email = $_POST['updatedEmail'] ?? '';
    $Telefon = $_POST['updatedNazevTelefon'] ?? '';

    error_log("Debug: ID=$ID, Jmenoprijmeni=$Jmenoprijmeni, Pozice=$Pozice, Mesto=$Mesto, Email=$Email, Telefon=$Telefon");

    if ($ID && $Jmenoprijmeni && $Pozice && $Mesto && $Email && $Telefon) {
        // Splitting the $Jmenoprijmeni into separate variables for jmeno and prijmeni
        list($jmeno, $prijmeni) = explode(' ', $Jmenoprijmeni, 2);

        // Získání ID_pozice z tabulky pozice na základě názvu pozice
        $sqlGetPoziceID = "SELECT ID_pozice FROM pozice WHERE nazev = ?";
        $stmtGetPoziceID = $conn->prepare($sqlGetPoziceID);

        if ($stmtGetPoziceID) {
            $stmtGetPoziceID->bind_param("s", $Pozice);
            $stmtGetPoziceID->execute();
            $stmtGetPoziceID->bind_result($ID_pozice);
            $stmtGetPoziceID->fetch();
            $stmtGetPoziceID->close();

            // Zkontrolujte, zda město existuje v tabulce adresa
            $sqlCheckMesto = "SELECT ID_adresa FROM adresa WHERE mesto = ?";
            $stmtCheckMesto = $conn->prepare($sqlCheckMesto);

            if ($stmtCheckMesto) {
                $Mesto = mysqli_real_escape_string($conn, $Mesto);
                $stmtCheckMesto->bind_param("s", $Mesto);
                $stmtCheckMesto->execute();
                $stmtCheckMesto->store_result();

                if ($stmtCheckMesto->num_rows == 0) {
                    // Město neexistuje, vytvořte nový záznam v tabulce adresa
                    $sqlInsertAdresa = "INSERT INTO adresa (mesto) VALUES ('$Mesto')";
                    if (!mysqli_query($conn, $sqlInsertAdresa)) {
                        sendErrorResponse("Error inserting data into 'adresa' table: " . mysqli_error($conn));
                    }

                    // Získání ID_adresa nově vloženého záznamu
                    $ID_adresa = mysqli_insert_id($conn);
                } else {
                    // Město již existuje, získání ID_adresa
                    $stmtCheckMesto->bind_result($ID_adresa);
                    $stmtCheckMesto->fetch();
                }

                $stmtCheckMesto->close();

                // Aktualizace nebo vložení zamestnance
                if ($ID !== '') {
                    // Aktualizace existujícího zamestnance
                    $sqlUpdateZamestnanec = "UPDATE zamestnanci 
                                            SET jmeno = '$jmeno', prijmeni = '$prijmeni', 
                                                ID_pozice = $ID_pozice, ID_adresa = $ID_adresa, 
                                                email = '$Email', telefon = '$Telefon'
                                            WHERE ID_zamestnanec = $ID";

                    if (mysqli_query($conn, $sqlUpdateZamestnanec)) {
                        echo json_encode(array("success" => true, "message" => "Employee updated successfully."));
                    } else {
                        sendErrorResponse("Error updating employee: " . mysqli_error($conn));
                    }
                } else {
                    // Vložení nového zamestnance
                    $sqlInsertZamestnanec = "INSERT INTO zamestnanci (jmeno, prijmeni, ID_pozice, ID_adresa, email, telefon) 
                                            VALUES ('$jmeno', '$prijmeni', $ID_pozice, $ID_adresa, '$Email', '$Telefon')";

                    if (mysqli_query($conn, $sqlInsertZamestnanec)) {
                        echo json_encode(array("success" => true, "message" => "Employee inserted successfully."));
                    } else {
                        sendErrorResponse("Error inserting data into 'zamestnanci' table: " . mysqli_error($conn));
                    }
                }
            } else {
                // Přidáváme do chybového logu
                error_log("Error preparing statement for adresa ID: " . $conn->error);

                sendErrorResponse("Error preparing statement for adresa ID: " . $conn->error);
            }
        } else {
            // Přidáváme do chybového logu
            error_log("Error preparing statement for pozice ID: " . $conn->error);

            sendErrorResponse("Error preparing statement for pozice ID: " . $conn->error);
        }
    } else {
        sendErrorResponse("All fields are required.");
    }
} else {
    sendErrorResponse("Invalid request method.");
}

mysqli_close($conn);
