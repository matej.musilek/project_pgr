<?php
include 'init.php';

$response = array('success' => false, 'message' => 'Unknown error');

if (isset($_POST['ID_zamestnanec'])) {
    $zamestnanecID = $_POST['ID_zamestnanec'];

    // Smazání všech záznamů v tabulce prirazeni odkazujících na zaměstnance
    $sqlDeletePrirazeni = "DELETE FROM prirazeni WHERE ID_zamestnanec = $zamestnanecID";

    if ($conn->query($sqlDeletePrirazeni) === TRUE) {
        // Nyní můžeme smazat zaměstnance a adresu
        $sqlGetAdresaID = "SELECT ID_adresa FROM zamestnanci WHERE ID_zamestnanec = $zamestnanecID";
        $resultGetAdresaID = $conn->query($sqlGetAdresaID);

        if ($resultGetAdresaID->num_rows > 0) {
            $rowAdresaID = $resultGetAdresaID->fetch_assoc();
            $adresaID = $rowAdresaID['ID_adresa'];

            // Smazání zaměstnance
            $sqlDeleteZamestnanec = "DELETE FROM zamestnanci WHERE ID_zamestnanec = $zamestnanecID";
            $sqlDeleteAdresa = "DELETE FROM adresa WHERE ID_adresa = $adresaID";

            if ($conn->query($sqlDeleteZamestnanec) === TRUE && $conn->query($sqlDeleteAdresa) === TRUE) {
                $response = array('success' => true, 'message' => 'Employee and related address deleted successfully');
            } else {
                $response = array('success' => false, 'message' => 'Error deleting employee or address: ' . $conn->error);
            }
        } else {
            $response = array('success' => false, 'message' => 'Error getting address ID');
        }
    } else {
        $response = array('success' => false, 'message' => 'Error deleting related entries in prirazeni: ' . $conn->error);
    }
} else {
    $response = array('success' => false, 'message' => 'Missing employee ID parameter');
}

$conn->close();

header('Content-Type: application/json');
echo json_encode($response);
