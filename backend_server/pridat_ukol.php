<?php
include 'init.php';

function sendErrorResponse($message)
{
    echo json_encode(array("success" => false, "message" => $message));
    exit;
}

$nazev = $_POST['nazev'] ?? '';
$termin = $_POST['termin'] ?? '';
$status = $_POST['status'] ?? '';
$popis = $_POST['popis'] ?? '';
$projekt = $_POST['projekt'] ?? '';
$jmenoPrijmeni = $_POST['zamestnanec'] ?? '';

if (!$nazev || !$termin || !$status || !$popis || !$projekt || !$jmenoPrijmeni) {
    sendErrorResponse("Required fields are missing");
}

// Zjistěte ID_projekty podle názvu projektu
$sqlProjekt = "SELECT ID_projekty FROM projekty WHERE nazev = '$projekt'";
$resultProjekt = $conn->query($sqlProjekt);

if ($resultProjekt === FALSE) {
    sendErrorResponse("Error: " . $conn->error);
}

$rowProjekt = $resultProjekt->fetch_assoc();
$idProjekt = $rowProjekt['ID_projekty'];

// Zjistěte ID_status podle názvu statusu
$sqlStatus = "SELECT ID_status FROM status WHERE stav = '$status'";
$resultStatus = $conn->query($sqlStatus);

if ($resultStatus === FALSE) {
    sendErrorResponse("Error: " . $conn->error);
}

$rowStatus = $resultStatus->fetch_assoc();
$idStatus = $rowStatus['ID_status'];

// Vložte nový záznam do tabulky ukoly
$sqlUkol = "INSERT INTO ukoly (nazev, termin, ID_status, popis, ID_projekty) 
            VALUES ('$nazev', '$termin', $idStatus, '$popis', $idProjekt)";

if ($conn->query($sqlUkol) === TRUE) {
    // Získání ID nově vytvořeného úkolu
    $idNovyUkol = $conn->insert_id;

    // Rozdělení pole s jménem a příjmením
    $jmenoPrijmeniArray = explode(" ", $jmenoPrijmeni);

    if (count($jmenoPrijmeniArray) == 2) {
        list($jmeno, $prijmeni) = $jmenoPrijmeniArray;

        // Získání ID_zamestnanec z tabulky zamestnanci na základě jména a příjmení
        $sqlGetZamestnanecID = "SELECT ID_zamestnanec FROM zamestnanci WHERE jmeno = '$jmeno' AND prijmeni = '$prijmeni'";
        $resultZamestnanecID = $conn->query($sqlGetZamestnanecID);

        if ($resultZamestnanecID === FALSE) {
            sendErrorResponse("Error: " . $conn->error);
        }

        $rowZamestnanecID = $resultZamestnanecID->fetch_assoc();
        $idZamestnanec = $rowZamestnanecID['ID_zamestnanec'];

        // Vložte nový záznam do tabulky prirazeni
        $sqlPrirazeni = "INSERT INTO prirazeni (ID_zamestnanec, ID_ukoly) 
                         VALUES ($idZamestnanec, $idNovyUkol)";

        if ($conn->query($sqlPrirazeni) === TRUE) {
            echo json_encode(array("success" => true, "message" => "New task and assignment records created successfully"));
        } else {
            sendErrorResponse("Error: " . $conn->error);
        }
    } else {
        sendErrorResponse("Invalid format for 'jmenoPrijmeni' field");
    }
} else {
    sendErrorResponse("Error: " . $conn->error);
}

$conn->close();
