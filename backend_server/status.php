<?php
include 'init.php';

$sql = "SELECT 
            s.ID_Status,
            s.stav      
        FROM status s";

$result = $conn->query($sql);

if ($result === FALSE) {
    die("Error executing query: " . $conn->error);
}

$stav = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $stav[] = $row;
    }
}

echo json_encode($stav);

$conn->close();
