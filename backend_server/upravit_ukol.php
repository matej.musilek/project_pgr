<?php
include 'init.php';

function sendSuccessResponse($message)
{
    echo json_encode(array("success" => true, "message" => $message));
    exit;
}

function sendErrorResponse($message)
{
    echo json_encode(array("success" => false, "message" => $message));
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ID = $_POST['updatedID_ukoly'] ?? '';
    $Nazev = $_POST['updatedNazev'] ?? '';
    $Termin = $_POST['updatedTermin'] ?? '';
    $StatusText = $_POST['updatedStatus'] ?? '';
    $Popis = $_POST['updatedPopis'] ?? '';
    $NazevProjektu = $_POST['updatedNazevProjektu'] ?? '';
    $JmenoPrijmeni = $_POST['updatedJmenoPrijmeni'] ?? '';

    // Přidáváme do chybového logu
    error_log("ID: $ID");
    error_log("Nazev: $Nazev");
    error_log("Termin: $Termin");
    error_log("StatusText: $StatusText");
    error_log("Popis: $Popis");
    error_log("NazevProjektu: $NazevProjektu");
    error_log("JmenoPrijmeni: $JmenoPrijmeni");

    if ($Nazev && $Termin && $StatusText && $Popis && $NazevProjektu && $ID && $JmenoPrijmeni) {
        // Rozdělení pole s jménem a příjmením
        $jmenoPrijmeniArray = explode(" ", $JmenoPrijmeni);

        if (count($jmenoPrijmeniArray) == 2) {
            list($jmeno, $prijmeni) = $jmenoPrijmeniArray;

            // Získání ID_zamestnanec z tabulky zamestnanci na základě jména a příjmení
            $sqlGetZamestnanecID = "SELECT ID_zamestnanec FROM zamestnanci WHERE jmeno = ? AND prijmeni = ?";
            $stmtGetZamestnanecID = $conn->prepare($sqlGetZamestnanecID);

            if ($stmtGetZamestnanecID) {
                $stmtGetZamestnanecID->bind_param("ss", $jmeno, $prijmeni);
                $stmtGetZamestnanecID->execute();
                $stmtGetZamestnanecID->bind_result($ID_zamestnanec);
                $stmtGetZamestnanecID->fetch();
                $stmtGetZamestnanecID->close();

                // Získání ID_status z tabulky status na základě textového statusu
                $sqlGetStatusID = "SELECT ID_status FROM status WHERE stav = ?";
                $stmtGetStatusID = $conn->prepare($sqlGetStatusID);

                if ($stmtGetStatusID) {
                    $stmtGetStatusID->bind_param("s", $StatusText);
                    $stmtGetStatusID->execute();
                    $stmtGetStatusID->bind_result($ID_status);
                    $stmtGetStatusID->fetch();
                    $stmtGetStatusID->close();

                    // Získání ID_projekty z tabulky projekty na základě názvu projektu
                    $sqlGetProjektyID = "SELECT ID_projekty FROM projekty WHERE nazev = ?";
                    $stmtGetProjektyID = $conn->prepare($sqlGetProjektyID);

                    if ($stmtGetProjektyID) {
                        $stmtGetProjektyID->bind_param("s", $NazevProjektu);
                        $stmtGetProjektyID->execute();
                        $stmtGetProjektyID->bind_result($ID_projekty);
                        $stmtGetProjektyID->fetch();
                        $stmtGetProjektyID->close();

                        // Aktualizace tabulky ukoly s ID_status a ID_projekty
                        $sqlUpdateUkoly = "UPDATE ukoly SET nazev = ?, termin = ?, ID_status = ?, popis = ?, ID_projekty = ? WHERE ID_ukoly = ?";
                        $stmtUkoly = $conn->prepare($sqlUpdateUkoly);

                        if ($stmtUkoly) {
                            $stmtUkoly->bind_param("ssssss", $Nazev, $Termin, $ID_status, $Popis, $ID_projekty, $ID);

                            if ($stmtUkoly->execute()) {
                                $stmtUkoly->close();

                                // Zkontrolujte, zda existuje záznam v tabulce prirazeni
                                $sqlCheckPrirazeni = "SELECT 1 FROM prirazeni WHERE ID_ukoly = ?";
                                $stmtCheckPrirazeni = $conn->prepare($sqlCheckPrirazeni);

                                if ($stmtCheckPrirazeni) {
                                    $stmtCheckPrirazeni->bind_param("s", $ID);
                                    $stmtCheckPrirazeni->execute();
                                    $stmtCheckPrirazeni->store_result();

                                    if ($stmtCheckPrirazeni->num_rows > 0) {
                                        // Pokud záznam existuje, aktualizujte pouze ID_zamestnanec
                                        $stmtCheckPrirazeni->close();

                                        // Aktualizace tabulky prirazeni s novým ID_zamestnanec
                                        $sqlUpdatePrirazeni = "UPDATE prirazeni SET ID_zamestnanec = ? WHERE ID_ukoly = ?";
                                        $stmtPrirazeni = $conn->prepare($sqlUpdatePrirazeni);

                                        if ($stmtPrirazeni) {
                                            $stmtPrirazeni->bind_param("ss", $ID_zamestnanec, $ID);

                                            if ($stmtPrirazeni->execute()) {
                                                $stmtPrirazeni->close();
                                                sendSuccessResponse("Task and assignment updated successfully");
                                            } else {
                                                // Přidáváme do chybového logu
                                                error_log("Error updating assignment: " . $stmtPrirazeni->error);

                                                sendErrorResponse("Error updating assignment: " . $stmtPrirazeni->error);
                                            }
                                        } else {
                                            // Přidáváme do chybového logu
                                            error_log("Error preparing statement for assignment: " . $conn->error);

                                            sendErrorResponse("Error preparing statement for assignment: " . $conn->error);
                                        }
                                    } else {
                                        $stmtCheckPrirazeni->close();

                                        // Vytvoření nového záznamu v tabulce prirazeni
                                        $sqlInsertPrirazeni = "INSERT INTO prirazeni (ID_ukoly, ID_zamestnanec) VALUES (?, ?)";
                                        $stmtInsertPrirazeni = $conn->prepare($sqlInsertPrirazeni);

                                        if ($stmtInsertPrirazeni) {
                                            $stmtInsertPrirazeni->bind_param("ss", $ID, $ID_zamestnanec);

                                            if ($stmtInsertPrirazeni->execute()) {
                                                $stmtInsertPrirazeni->close();
                                                sendSuccessResponse("Task and assignment updated successfully");
                                            } else {
                                                // Přidáváme do chybového logu
                                                error_log("Error inserting assignment: " . $stmtInsertPrirazeni->error);

                                                sendErrorResponse("Error inserting assignment: " . $stmtInsertPrirazeni->error);
                                            }
                                        } else {
                                            // Přidáváme do chybového logu
                                            error_log("Error preparing statement for assignment: " . $conn->error);

                                            sendErrorResponse("Error preparing statement for assignment: " . $conn->error);
                                        }
                                    }
                                } else {
                                    // Přidáváme do chybového logu
                                    error_log("Error preparing statement for checking assignment: " . $conn->error);

                                    sendErrorResponse("Error preparing statement for checking assignment: " . $conn->error);
                                }
                            } else {
                                // Přidáváme do chybového logu
                                error_log("Error updating task: " . $stmtUkoly->error);

                                sendErrorResponse("Error updating task: " . $stmtUkoly->error);
                            }
                        } else {
                            // Přidáváme do chybového logu
                            error_log("Error preparing statement for task: " . $conn->error);

                            sendErrorResponse("Error preparing statement for task: " . $conn->error);
                        }
                    } else {
                        // Přidáváme do chybového logu
                        error_log("Error preparing statement for project ID: " . $conn->error);

                        sendErrorResponse("Error preparing statement for project ID: " . $conn->error);
                    }
                } else {
                    // Přidáváme do chybového logu
                    error_log("Error preparing statement for status ID: " . $conn->error);

                    sendErrorResponse("Error preparing statement for status ID: " . $conn->error);
                }
            } else {
                // Přidáváme do chybového logu
                error_log("Error preparing statement for employee ID: " . $conn->error);

                sendErrorResponse("Error preparing statement for employee ID: " . $conn->error);
            }
        } else {
            sendErrorResponse("Invalid format for 'JmenoPrijmeni' field");
        }
    } else {
        sendErrorResponse("Required fields for updating are missing");
    }
} else {
    sendErrorResponse("Invalid request method");
}

$conn->close();
