import React from "react";

import { Routes, Route, useLocation } from "react-router-dom";
import { AnimatePresence } from "framer-motion";
import $ from "jquery";

import Home from "../../../src/pages/home/home";

function AnimatedRoutes() {
  const location = useLocation();
  return (
    <>
      <AnimatePresence>
        <Routes location={location} key={location.pathname}>
          <Route exact path="/" element={<Home />} />
        </Routes>
      </AnimatePresence>
    </>
  );
}

export default AnimatedRoutes;
