import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./home.css";

const Home = () => {
  // Navbar

  const [isSidebarActive, setIsSidebarActive] = useState(false);
  const [sidebarActiveClass, setSidebarActiveClass] = useState(1);
  const handleMenuBtnClick = () => {
    setIsSidebarActive((prevIsSidebarActive) => !prevIsSidebarActive);
  };

  // Načíst data

  const [ukol, setUkol] = useState([]);
  const [employees, setEmployees] = useState([]);
  const [project, setProject] = useState([]);
  const [status, setStatus] = useState([]);
  const [prehled, setPrehled] = useState([]);
  const [pozice, setPozice] = useState([]);
  const [zamestnanci, setZamestnanci] = useState([]);

  // Přidat data

  const [showAddUkolForm, setShowAddUkolForm] = useState(false);
  const [showAddProjectForm, setShowAddProjectForm] = useState(false);
  const [showAddZamestnanecForm, setShowAddZamestnanecForm] = useState(false);

  // Upravit data

  const [isEditing, setIsEditing] = useState(false);
  const [isEditingUkol, setIsEditingUkol] = useState(false);
  const [isEditingZamestnanec, setIsEditingZamestnanec] = useState(false);
  const [selectedProject, setSelectedProject] = useState(null);
  const [selectedUkol, setSelectedUkol] = useState(null);
  const [selectedZamestnanec, setSelectedZamestnanec] = useState(null);

  const [selectedProjectData, setSelectedProjectData] = useState({
    nazev: "",
    popis: "",
    od: "",
    do: "",
  });

  const [SelectedUkolData, setSelectedUkolData] = useState({
    nazev: "",
    termin: "",
    status: "",
    popis: "",
    projekt: "",
    zamestnanec: "",
  });

  const [SelectedZamestnanecData, setSelectedZamestnanecData] = useState({
    jmenoprijmeni: "",
    pozice: "",
    ulice: "",
    cp: "",
    PSC: "",
    mesto: "",
    email: "",
    telefon: "",
  });

  const [newProjectData, setNewProjectData] = useState({
    nazev: "",
    popis: "",
    od: "",
    do: "",
  });

  const [newUkolData, setNewUkolData] = useState({
    nazev: "",
    termin: "",
    status: "",
    popis: "",
    projekt: "",
    zamestnanec: "",
  });

  const [newZamestnanecData, setNewZamestnanecData] = useState({
    jmenoprijmeni: "",
    pozice: "",
    ulice: "",
    cp: "",
    PSC: "",
    mesto: "",
    email: "",
    telefon: "",
  });

  // Načíst data

  const fetchUkol = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/ukoly.php")
      .then((response) => response.json())
      .then((data) => setUkol(data))
      .catch((error) => console.error("Error fetching ukol:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchUkol, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const fetchPrehled = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/prehled.php")
      .then((response) => response.json())
      .then((data) => setPrehled(data))
      .catch((error) => console.error("Error fetching ukol:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchPrehled, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const fetchProject = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/projekt.php")
      .then((response) => response.json())
      .then((data) => setProject(data))
      .catch((error) => console.error("Error fetching ukol:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchProject, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const fetchEmployees = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/zamestnanci.php")
      .then((response) => response.json())
      .then((data) => setEmployees(data))
      .catch((error) => console.error("Error fetching ukol:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchEmployees, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const fetchStatus = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/status.php")
      .then((response) => response.json())
      .then((data) => setStatus(data))
      .catch((error) => console.error("Error fetching status:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchStatus, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const fetchPozice = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/pozice.php")
      .then((response) => response.json())
      .then((data) => setPozice(data))
      .catch((error) => console.error("Error fetching status:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchPozice, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const fetchZamestnanci = () => {
    fetch("https://musilek.soskolin.eu/server_projectpgr/zamestnanci_list.php")
      .then((response) => response.json())
      .then((data) => setZamestnanci(data))
      .catch((error) => console.error("Error fetching status:", error));
  };

  useEffect(() => {
    const intervalId = setInterval(fetchZamestnanci, 1000);
    return () => clearInterval(intervalId);
  }, []);

  // Přidat data

  const handleProjectInputChange = (e) => {
    const { name, value } = e.target;
    if (isEditing) {
      setSelectedProjectData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    } else {
      setNewProjectData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
    console.log(setSelectedProjectData);
  };

  const handleUkolInputChange = (e) => {
    const { name, value } = e.target;
    if (isEditingUkol) {
      setSelectedUkolData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    } else {
      setNewUkolData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }

    console.log(setSelectedUkolData);
  };

  const handleZamestnanciInputChange = (e) => {
    const { name, value } = e.target;
    if (isEditingZamestnanec) {
      setSelectedZamestnanecData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    } else {
      setNewZamestnanecData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }

    console.log(setSelectedZamestnanecData);
  };

  const toggleAddUkolForm = () => {
    setShowAddUkolForm((prev) => !prev);
  };

  const toggleAddProjectForm = () => {
    setShowAddProjectForm((prev) => !prev);
  };

  const toggleAddZamestnanciForm = () => {
    setShowAddZamestnanecForm((prev) => !prev);
  };

  const handleAddProject = () => {
    const formData = new FormData();

    for (const key in newProjectData) {
      formData.append(key, newProjectData[key]);
    }

    fetch("https://musilek.soskolin.eu/server_projectpgr/pridat_projekt.php", {
      method: "POST",
      body: formData,
    })
      .then(async (response) => {
        console.log("Server response:", response);

        if (!response.ok) {
          throw new Error(`Server returned status: ${response.status}`);
        }

        try {
          const contentType = response.headers.get("content-type");

          if (contentType && contentType.includes("application/json")) {
            return response.json();
          } else {
            throw new Error("Invalid content type");
          }
        } catch (error) {
          console.error("Error parsing JSON:", error);
          return { success: false, message: "Invalid response format" };
        }
      })
      .then((data) => {
        if (data.success) {
          console.log("Pacient byl úspěšně přidán:", data.message);
          toast.success(`Pacient byl úspěšně přidán.`);
          toggleAddProjectForm(false);
          fetchProject();
        } else {
          console.error("Chyba při přidávání pacienta:", data.message);
          toast.error(`Chyba při přidávání pacienta.`);
        }
      })
      .catch((error) => {
        console.error("Error:", error.message);
        toast.error(`Chyba při přidávání pacienta: ${error.message}`);
      });

    setNewProjectData({
      nazev: "",
      popis: "",
      od: "",
      do: "",
    });
  };

  const handleAddUkol = async () => {
    console.log("Frontend Data before sending:", newUkolData);
    try {
      const formData = new FormData();

      for (const key in newUkolData) {
        formData.append(key, newUkolData[key]);
      }

      console.log("Frontend FormData:", formData);

      console.log("Frontend Data before sending:", newUkolData);

      const response = await fetch("https://musilek.soskolin.eu/server_projectpgr/pridat_ukol.php", {
        method: "POST",
        body: formData,
      });

      console.log("Frontend Response Status:", response.status);

      if (!response.ok) {
        throw new Error(`Server returned status: ${response.status}`);
      }

      const contentType = response.headers.get("content-type");

      if (contentType && contentType.includes("application/json")) {
        const data = await response.json();
        console.log("Frontend Server Response:", data);

        if (data.success) {
          toggleAddUkolForm(false);
          fetchUkol();
          console.log("Úkol byl úspěšně přidán:", data.message);
          toast.success("Úkol byl úspěšně přidán:", data.message);
        } else {
          console.error("Chyba při přidávání úkolu:", data.message);
          toast.error("Chyba při přidávání úkolu:", data.message);
        }
      } else {
        throw new Error("Invalid content type");
      }
    } catch (error) {
      console.error("Frontend Error:", error.message);
    }

    setNewUkolData({
      nazev: "",
      termin: "",
      status: "",
      popis: "",
      projekt: "",
      zamestnanec: "",
    });
  };

  const handleAddzamestnanec = async () => {
    console.log("Frontend Data before sending:", newZamestnanecData);
    try {
      const formData = new FormData();

      for (const key in newZamestnanecData) {
        formData.append(key, newZamestnanecData[key]);
      }

      console.log("Frontend FormData:", formData);

      console.log("Frontend Data before sending:", newZamestnanecData);

      const response = await fetch("https://musilek.soskolin.eu/server_projectpgr/pridat_zamestnance.php", {
        method: "POST",
        body: formData,
      });

      console.log("Frontend Response Status:", response.status);

      if (!response.ok) {
        throw new Error(`Server returned status: ${response.status}`);
      }

      const contentType = response.headers.get("content-type");

      if (contentType && contentType.includes("application/json")) {
        const data = await response.json();
        console.log("Frontend Server Response:", data);

        if (data.success) {
          toggleAddZamestnanciForm(false);
          fetchEmployees();
          console.log("Zaměstnanec byl úspěšně přidán:", data.message);
          toast.success("Zaměstnanec byl úspěšně přidán:", data.message);
        } else {
          console.error("Chyba při přidávání zaměstnanec:", data.message);
          toast.error("Chyba při přidávání zaměstnance:", data.message);
        }
      } else {
        throw new Error("Invalid content type");
      }
    } catch (error) {
      console.error("Frontend Error:", error.message);
    }

    setNewZamestnanecData({
      jmenoprijmeni: "",
      pozice: "",
      ulice: "",
      cp: "",
      PSC: "",
      mesto: "",
      email: "",
      telefon: "",
    });
  };

  // upravit data

  const handleProjectTableRowClick = (project) => {
    setSelectedProject(project);
    setIsEditing(true);

    setSelectedProjectData({
      ID_projekty: project.ID_projekty,
      nazev: project.nazev,
      popis: project.popis,
      od: project.od,
      do: project.do,
    });
  };

  const handleUpdateProject = () => {
    console.log("Selected Project Data:", selectedProjectData);
    fetch("https://musilek.soskolin.eu/server_projectpgr/uprav_projekt.php", {
      method: "POST",
      body: new URLSearchParams({
        updatedID: selectedProjectData.ID_projekty,
        updateProject: true,
        updatedNazev: selectedProjectData.nazev,
        updatedPopis: selectedProjectData.popis,
        updatedOd: selectedProjectData.od,
        updatedDo: selectedProjectData.do,
      }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          toast.success("Project updated successfully");
          setIsEditing(false);
          setSelectedProject(null);
          fetchProject();
          console.log(handleUpdateProject);
        } else {
          toast.error("Error updating project");
        }
      })
      .catch((error) => {
        console.error("Error updating project:", error);
        toast.error(`Error updating project: ${error.message}`);
      });
  };

  const handleTaskTableRowClick = (tasks) => {
    setSelectedUkol(tasks);
    setIsEditingUkol(true);

    setSelectedUkolData({
      ID_ukoly: tasks.ID_ukoly,
      nazev: tasks.nazev,
      termin: tasks.termin,
      status: tasks.status,
      popis: tasks.popis,
      projekt: tasks.projekty,
      zamestnanec: tasks.jmeno_zamestnance + " " + tasks.prijmeni_zamestnance,
    });
  };

  const handleUpdateTask = () => {
    console.log("Selected Project Data:", SelectedUkolData);
    fetch("https://musilek.soskolin.eu/server_projectpgr/upravit_ukol.php", {
      method: "POST",
      body: new URLSearchParams({
        updateUkoly: true,
        updatedID_ukoly: SelectedUkolData.ID_ukoly,
        updatedNazev: SelectedUkolData.nazev,
        updatedTermin: SelectedUkolData.termin,
        updatedStatus: SelectedUkolData.status,
        updatedPopis: SelectedUkolData.popis,
        updatedNazevProjektu: SelectedUkolData.projekt,
        updatedJmenoPrijmeni: SelectedUkolData.zamestnanec,
      }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          toast.success("Task updated successfully");
          setIsEditingUkol(false);
          setSelectedUkol(null);
          fetchUkol();
          console.log(handleUpdateTask);
        } else {
          toast.error("Error updating project");
        }
      })
      .catch((error) => {
        console.error("Error updating project:", error);
        toast.error(`Error updating project: ${error.message}`);
      });
  };

  const handleZamestnanciTableRowClick = (employees) => {
    setSelectedZamestnanec(employees);
    setIsEditingZamestnanec(true);

    setSelectedZamestnanecData({
      ID_zamestnanec: employees.ID_zamestnanec,
      jmenoprijmeni: employees.jmeno + " " + employees.prijmeni,
      pozice: employees.pozice,
      mesto: employees.mesto,
      email: employees.email,
      telefon: employees.telefon,
    });
  };

  const handleUpdateZamestnance = () => {
    console.log("Selected Project Data:", SelectedZamestnanecData);
    fetch("https://musilek.soskolin.eu/server_projectpgr/upravit_zamestnance.php", {
      method: "POST",
      body: new URLSearchParams({
        updateZamestnance: true,
        updatedID_zamestnanec: SelectedZamestnanecData.ID_zamestnanec,
        updatedJmenoprijmeni: SelectedZamestnanecData.jmenoprijmeni,
        updatedPozice: SelectedZamestnanecData.pozice,
        updatedMesto: SelectedZamestnanecData.mesto,
        updatedEmail: SelectedZamestnanecData.email,
        updatedNazevTelefon: SelectedZamestnanecData.telefon,
      }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          toast.success("Employee updated successfully");
          setIsEditingZamestnanec(false);
          setSelectedZamestnanec(null);
          fetchEmployees();
          console.log(handleUpdateZamestnance);
        } else {
          toast.error("Error updating employee");
        }
      })
      .catch((error) => {
        console.error("Error updating employee:", error);
        toast.error(`Error updating employee: ${error.message}`);
      });
  };

  // odstranit data

  const handleDeleteProject = () => {
    if (selectedProject && selectedProject.ID_projekty) {
      console.log("Deleting project with ID:", selectedProject.ID_projekty);

      fetch("https://musilek.soskolin.eu/server_projectpgr/odebrat_projekt.php", {
        method: "POST",
        body: new URLSearchParams({
          deletePatient: true,
          projectID: selectedProject.ID_projekty,
        }),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
        .then((response) => {
          console.log("Response received from server");
          if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          console.log("Response data:", data);
          if (data.success) {
            toast.success("Project deleted successfully");
            setSelectedProject(null);
            fetchProject();
          } else {
            toast.error(`Error deleting project: ${data.message}`);
          }
        })
        .catch((error) => {
          console.error("Error deleting project:", error);
          toast.error(`Error deleting project: ${error.message}`);
        });
    } else {
      console.error("No project selected or missing ID");
      toast.error("No project selected or missing ID");
    }
  };

  const handleDeleteUkol = () => {
    if (selectedUkol && selectedUkol.ID_ukoly) {
      console.log("Deleting project with ID:", selectedUkol.ID_ukoly);

      fetch("https://musilek.soskolin.eu/server_projectpgr/odebrat_ukol.php", {
        method: "POST",
        body: new URLSearchParams({
          deletePatient: true,
          ukolID: selectedUkol.ID_ukoly,
        }),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
        .then((response) => {
          console.log("Response received from server");
          if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          console.log("Response data:", data);
          if (data.success) {
            toast.success("Task deleted successfully");
            setSelectedUkol(null);
            fetchUkol();
          } else {
            toast.error(`Error deleting task: ${data.message}`);
          }
        })
        .catch((error) => {
          console.error("Error deleting task:", error);
          toast.error(`Error deleting task: ${error.message}`);
        });
    } else {
      console.error("No task selected or missing ID");
      toast.error("No task selected or missing ID");
    }
  };

  const handleDeleteZamestnanec = () => {
    if (selectedZamestnanec && selectedZamestnanec.ID_zamestnanec) {
      console.log("Deleting project with ID:", selectedZamestnanec.ID_zamestnanec);

      fetch("https://musilek.soskolin.eu/server_projectpgr/odebrat_zamestnance.php", {
        method: "POST",
        body: new URLSearchParams({
          deletePatient: true,
          ID_zamestnanec: selectedZamestnanec.ID_zamestnanec,
        }),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
        .then((response) => {
          console.log("Response received from server");
          if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          console.log("Response data:", data);
          if (data.success) {
            toast.success("Employee deleted successfully");
            setSelectedZamestnanec(null);
            fetchEmployees();
          } else {
            toast.error(`Error deleting project: ${data.message}`);
          }
        })
        .catch((error) => {
          console.error("Error deleting project:", error);
          toast.error(`Error deleting project: ${error.message}`);
        });
    } else {
      console.error("No project selected or missing ID");
      toast.error("No project selected or missing ID");
    }
  };
  return (
    <>
      <Helmet>
        <title>Database | admin</title>
        <meta name="description" content="Lorem ipsum dolor sit amet" />
      </Helmet>
      <ToastContainer position="top-right" autoClose={1000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
      <section className="main">
        <div className={`sidebar ${isSidebarActive ? "active" : ""}`}>
          <div className="top">
            <div className="logo">
              <img src=".././images/logo.png" draggable="false" alt="Logo" />
            </div>
            <i class="bx bx-menu" id="menu_btn" onClick={handleMenuBtnClick}></i>
          </div>
          <ul>
            <li onClick={() => setSidebarActiveClass(1)}>
              <a href="#">
                <i class="bx bx-line-chart"></i>
                <span className="nav_item">Přehled</span>
              </a>
            </li>

            <li onClick={() => setSidebarActiveClass(2)}>
              <a href="#">
                <i class="bx bx-spreadsheet"></i>
                <span className="nav_item">Projekty</span>
              </a>
            </li>

            <li onClick={() => setSidebarActiveClass(3)}>
              <a href="#">
                <i class="bx bx-task"></i>
                <span className="nav_item">Úkoly</span>
              </a>
            </li>

            <li onClick={() => setSidebarActiveClass(4)}>
              <a href="#">
                <i class="bx bx-briefcase"></i>
                <span className="nav_item">Zaměstnanci</span>
              </a>
            </li>
            <li id="lastchild">
              <a href="https://www.google.com/">
                <i class="bx bx-log-out-circle"></i>
                <span className="nav_item">Logout</span>
              </a>
            </li>
          </ul>
        </div>
        <div className="main_content">
          <div className="container">
            {sidebarActiveClass && sidebarActiveClass === 1 && (
              <>
                <section className="section_userdatabase">
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      marginTop: "3rem",
                    }}
                  >
                    <h5>PŘEHLED</h5>
                  </div>
                  <hr></hr>
                  <div class="table-responsive py-5">
                    <table class="table table-bordered table-hover">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Název Projektu</th>
                          <th scope="col">Přiřazený zaměstnanec</th>
                          <th scope="col">Název úkolu</th>
                          <th scope="col">Status úkolu</th>
                          <th scope="col">Termín úkolu</th>
                        </tr>
                      </thead>
                      <tbody>
                        {prehled.map((overview) => (
                          <tr key={overview.ID_ukoly}>
                            <th scope="row">{overview.ID_ukoly}</th>
                            <td>{overview.projekt}</td>
                            <td>{overview.jmeno_zamestnance + " " + overview.prijmeni_zamestnance}</td>
                            <td>{overview.nazev}</td>
                            <td>{overview.status}</td>
                            <td>{overview.termin}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </section>
              </>
            )}

            {sidebarActiveClass && sidebarActiveClass === 2 && (
              <>
                <section className="section_userdatabase">
                  {showAddProjectForm && (
                    <div className="container">
                      <div className="row">
                        <form className="adduser_form">
                          <div className="col-md-6">
                            <label for="nazevProjektu">Název projektu</label>
                            <input id="nazevProjektu" className="adduser_input" type="text" placeholder="Název projektu" name="nazev" value={newProjectData.nazev} onChange={handleProjectInputChange}></input>
                            <label for="popisProjektu">Popis projektu</label>
                            <input id="popisProjektu" className="adduser_input" type="text" placeholder="Popis projektu" name="popis" value={newProjectData.popis} onChange={handleProjectInputChange}></input>
                          </div>
                          <div className="col-md-6">
                            <label for="zacatekProjektu">Začátek projektu</label>
                            <input className="adduser_input" type="date" id="zacatekProjektu" name="od" value={newProjectData.od} onChange={handleProjectInputChange}></input>
                            <label for="konecProjektu">Konec projektu</label>
                            <input className="adduser_input" type="date" id="konecProjektu" name="do" value={newProjectData.do} onChange={handleProjectInputChange}></input>
                          </div>
                        </form>
                        <div className="container" style={{ display: "flex", justifyContent: "flex-end" }}>
                          <button type="button" onClick={handleAddProject} className="btn-adduser">
                            Přidat projekt
                          </button>
                        </div>
                      </div>
                    </div>
                  )}
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      marginTop: "3rem",
                    }}
                  >
                    <h5>PROJEKTY</h5>
                    <h5>
                      {" "}
                      <a onClick={toggleAddProjectForm}>
                        <i style={{ color: "#4BB543", fontSize: "1.5rem" }} className="bx bx-plus"></i>
                      </a>
                    </h5>
                  </div>
                  <hr></hr>
                  <div class="table-responsive py-5">
                    <table class="table table-bordered table-hover">
                      <thead class="thead-dark">
                        {selectedProject && (
                          <tr>
                            <th scope="col" style={{ display: "flex" }}>
                              <button
                                type="button"
                                className="btn-adduser"
                                onClick={handleUpdateProject}
                                style={{
                                  backgroundColor: "orange",
                                  height: "2rem",
                                }}
                              >
                                Aktualizovat
                              </button>
                              <button
                                type="button"
                                className="btn-adduser"
                                onClick={handleDeleteProject}
                                style={{
                                  backgroundColor: "red",
                                  height: "2rem",
                                  marginLeft: "1rem",
                                }}
                              >
                                <i class="bx bxs-trash-alt"></i>
                              </button>
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Název" name="nazev" value={isEditing ? selectedProjectData.nazev : ""} onChange={handleProjectInputChange} />
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Popis" name="popis" value={isEditing ? selectedProjectData.popis : ""} onChange={handleProjectInputChange} />
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Začátek projektu" name="od" value={isEditing ? selectedProjectData.od : ""} onChange={handleProjectInputChange} />
                            </th>

                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Konec projektu" name="do" value={isEditing ? selectedProjectData.do : ""} onChange={handleProjectInputChange} />
                            </th>
                          </tr>
                        )}
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Název</th>
                          <th scope="col">Popis</th>
                          <th scope="col">Začátek projektu</th>
                          <th scope="col">Konec projektu</th>
                        </tr>
                      </thead>
                      <tbody>
                        {project.map((project) => (
                          <tr key={project.ID_projekty} onClick={() => handleProjectTableRowClick(project)}>
                            <th scope="row">{project.ID_projekty}</th>
                            <td>{project.nazev}</td>
                            <td>{project.popis}</td>
                            <td>{project.od}</td>
                            <td>{project.do}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </section>
              </>
            )}

            {sidebarActiveClass && sidebarActiveClass === 3 && (
              <>
                <section className="section_userdatabase">
                  {showAddUkolForm && (
                    <div className="container">
                      <div className="row">
                        <form className="adduser_form">
                          <div className="col-md-6">
                            <label for="nazevProjektu">Název úkolu</label>
                            <input id="nazevProjektu" className="adduser_input" type="text" placeholder="Název úkolu" name="nazev" value={newUkolData.nazev} onChange={handleUkolInputChange}></input>
                            <label for="popisProjektu">Termín úkolu</label>
                            <input id="popisProjektu" className="adduser_input" type="date" placeholder="Termín úkolu" name="termin" value={newUkolData.termin} onChange={handleUkolInputChange}></input>
                            <label for="popisProjektu">Status úkolu</label>
                            <select name="status" id="status-select" value={newUkolData.status} onChange={handleUkolInputChange}>
                              <option value="">-- Vyber status --</option>
                              {status.map((stav) => (
                                <option key={stav.ID_status} value={stav.stav}>
                                  {stav.stav}
                                </option>
                              ))}
                            </select>
                          </div>
                          <div className="col-md-6">
                            <label for="popisUkolu">Popis úkolu</label>
                            <input className="adduser_input" type="text" id="popisUkolu" name="popis" value={newUkolData.popis} onChange={handleUkolInputChange}></input>
                            <label for="konecProjektu">Přiřazený projekt</label>
                            <select name="projekt" id="status-select" value={newUkolData.project} onChange={handleUkolInputChange}>
                              <option value="">-- Vyber projekt --</option>
                              {project.map((project) => (
                                <option key={project.ID_projekry} value={project.nazev}>
                                  {project.nazev}
                                </option>
                              ))}
                            </select>
                            <label for="konecProjektu">Přiřazený zaměstnanec</label>
                            <select className="adduser_input" name="zamestnanec" id="status-select" value={newUkolData.zamestnanec} onChange={handleUkolInputChange}>
                              <option value="">-- Vyber zaměstnance --</option>
                              {employees.map((employees) => (
                                <option key={employees.ID_zamestnanec} value={employees.jmeno + " " + employees.prijmeni}>
                                  {employees.jmeno + " " + employees.prijmeni}
                                </option>
                              ))}
                            </select>
                          </div>
                        </form>
                        <div className="container" style={{ display: "flex", justifyContent: "flex-end" }}>
                          <button type="button" onClick={handleAddUkol} className="btn-adduser">
                            Přidat úkol
                          </button>
                        </div>
                      </div>
                    </div>
                  )}
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      marginTop: "3rem",
                    }}
                  >
                    <h5>ÚKOLY</h5>
                    <h5>
                      {" "}
                      <a onClick={toggleAddUkolForm}>
                        <i style={{ color: "#4BB543", fontSize: "1.5rem" }} className="bx bx-plus"></i>
                      </a>
                    </h5>
                  </div>
                  <hr></hr>
                  <div class="table-responsive py-5">
                    <table class="table table-bordered table-hover">
                      <thead class="thead-dark">
                        {selectedUkol && (
                          <tr>
                            <th scope="col" style={{ display: "flex" }}>
                              <button
                                type="button"
                                className="btn-adduser"
                                onClick={handleUpdateTask}
                                style={{
                                  backgroundColor: "orange",
                                  height: "2rem",
                                }}
                              >
                                Aktualizovat
                              </button>
                              <button
                                type="button"
                                className="btn-adduser"
                                onClick={handleDeleteUkol}
                                style={{
                                  backgroundColor: "red",
                                  height: "2rem",
                                  marginLeft: "1rem",
                                }}
                              >
                                <i class="bx bxs-trash-alt"></i>
                              </button>
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Název" name="nazev" value={isEditingUkol ? SelectedUkolData.nazev : ""} onChange={handleUkolInputChange} />
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Popis" name="termin" value={isEditingUkol ? SelectedUkolData.termin : ""} onChange={handleUkolInputChange} />
                            </th>
                            <th scope="col">
                              <select className="adduser_input editadduser_input" name="status" id="status-select" value={isEditingUkol ? SelectedUkolData.status : ""} onChange={handleUkolInputChange}>
                                <option value="">-- Vyber status --</option>
                                {status.map((stav) => (
                                  <option key={stav.ID_status} value={stav.stav}>
                                    {stav.stav}
                                  </option>
                                ))}
                              </select>
                            </th>

                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" placeholder="Popis" name="popis" value={isEditingUkol ? SelectedUkolData.popis : ""} onChange={handleUkolInputChange} />
                            </th>
                            <th>
                              <select className="adduser_input editadduser_input" name="projekt" id="status-select" value={isEditingUkol ? SelectedUkolData.projekt : ""} onChange={handleUkolInputChange}>
                                <option value="">-- Vyber projekt --</option>
                                {project.map((project) => (
                                  <option key={project.ID_projekry} value={project.nazev}>
                                    {project.nazev}
                                  </option>
                                ))}
                              </select>
                            </th>

                            <th scope="col">
                              <select className="adduser_input editadduser_input" name="zamestnanec" id="status-select" value={isEditingUkol ? SelectedUkolData.zamestnanec : ""} onChange={handleUkolInputChange}>
                                <option value="">-- Vyber zaměstnance --</option>
                                {zamestnanci.map((zamestnanec) => (
                                  <option key={zamestnanec.ID_zamestnanec} value={zamestnanec.jmeno + " " + zamestnanec.prijmeni}>
                                    {`${zamestnanec.jmeno} ${zamestnanec.prijmeni}`}
                                  </option>
                                ))}
                              </select>
                            </th>
                          </tr>
                        )}
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Název</th>
                          <th scope="col">Termín</th>
                          <th scope="col">Status</th>
                          <th scope="col">Popis</th>
                          <th scope="col">Přiřazený projekt</th>
                          <th scope="col">Přiřazený zaměstnanec</th>
                        </tr>
                      </thead>
                      <tbody>
                        {ukol.map((tasks) => (
                          <tr key={tasks.ID_ukoly} onClick={() => handleTaskTableRowClick(tasks)}>
                            <th scope="row">{tasks.ID_ukoly}</th>
                            <td>{tasks.nazev}</td>
                            <td>{tasks.termin}</td>
                            <td>{tasks.status}</td>
                            <td>{tasks.popis}</td>
                            <td>{tasks.projekty}</td>
                            <td>{tasks.jmeno_zamestnance && tasks.prijmeni_zamestnance ? `${tasks.jmeno_zamestnance} ${tasks.prijmeni_zamestnance}` : "Není přiřazeno"}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </section>
              </>
            )}

            {sidebarActiveClass && sidebarActiveClass === 4 && (
              <>
                <section className="section_userdatabase">
                  {showAddZamestnanecForm && (
                    <div className="container">
                      <div className="row">
                        <form className="adduser_form">
                          <div className="col-md-6">
                            <label for="nazevProjektu">Jmeno a přijmení</label>
                            <input id="jmenoPrijmeniZamestnance" className="adduser_input" type="text" placeholder="Jmeno a přijmení" name="jmenoprijmeni" value={newZamestnanecData.jmenoprijmeni} onChange={handleZamestnanciInputChange}></input>
                            <label for="nazevProjektu">Pozice</label>
                            <select name="pozice" id="status-select" value={newZamestnanecData.pozice} onChange={handleZamestnanciInputChange}>
                              <option value="">-- Vyber pozici --</option>
                              {pozice.map((pozice) => (
                                <option key={pozice.ID_pozice} value={pozice.nazev}>
                                  {pozice.nazev}
                                </option>
                              ))}
                            </select>

                            <label for="ulice">Ulice</label>
                            <input id="ulice" className="adduser_input" type="text" placeholder="Ulice" name="ulice" value={newZamestnanecData.ulice} onChange={handleZamestnanciInputChange}></input>

                            <label for="cp">Číslo popisné</label>
                            <input id="cp" className="adduser_input" type="text" placeholder="Číslo popisné" name="cp" value={newZamestnanecData.cp} onChange={handleZamestnanciInputChange}></input>
                          </div>
                          <div className="col-md-6">
                            <label for="PSC">PSČ</label>
                            <input id="PSC" className="adduser_input" type="text" placeholder="PSČ" name="PSC" value={newZamestnanecData.PSC} onChange={handleZamestnanciInputChange}></input>

                            <label for="mesto">Město</label>
                            <input id="mesto" className="adduser_input" type="text" placeholder="Město" name="mesto" value={newZamestnanecData.mesto} onChange={handleZamestnanciInputChange}></input>

                            <label for="email">Email</label>
                            <input id="email" className="adduser_input" type="text" placeholder="Email" name="email" value={newZamestnanecData.email} onChange={handleZamestnanciInputChange}></input>

                            <label for="telefon">Telefon</label>
                            <input id="telefon" className="adduser_input" type="text" placeholder="Telefon" name="telefon" value={newZamestnanecData.telefon} onChange={handleZamestnanciInputChange}></input>
                          </div>
                        </form>
                        <div className="container" style={{ display: "flex", justifyContent: "flex-end" }}>
                          <button type="button" onClick={handleAddzamestnanec} className="btn-adduser">
                            Přidat zaměstnance
                          </button>
                        </div>
                      </div>
                    </div>
                  )}
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      marginTop: "3rem",
                    }}
                  >
                    <h5>ZAMĚSTNANCI</h5>
                    <h5>
                      {" "}
                      <a onClick={toggleAddZamestnanciForm}>
                        <i style={{ color: "#4BB543", fontSize: "1.5rem" }} className="bx bx-plus"></i>
                      </a>
                    </h5>
                  </div>
                  <hr></hr>
                  <div class="table-responsive py-5">
                    <table class="table table-bordered table-hover">
                      <thead class="thead-dark">
                        {selectedZamestnanec && (
                          <tr>
                            <th scope="col" style={{ display: "flex" }}>
                              <button
                                type="button"
                                className="btn-adduser"
                                onClick={handleUpdateZamestnance}
                                style={{
                                  backgroundColor: "orange",
                                  height: "2rem",
                                }}
                              >
                                Aktualizovat
                              </button>
                              <button
                                type="button"
                                className="btn-adduser"
                                onClick={handleDeleteZamestnanec}
                                style={{
                                  backgroundColor: "red",
                                  height: "2rem",
                                  marginLeft: "1rem",
                                }}
                              >
                                <i class="bx bxs-trash-alt"></i>
                              </button>
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" name="jmenoprijmeni" value={isEditingZamestnanec ? SelectedZamestnanecData.jmenoprijmeni : ""} onChange={handleZamestnanciInputChange} />
                            </th>
                            <th scope="col">
                              <select className="adduser_input editadduser_input" name="pozice" value={isEditingZamestnanec ? SelectedZamestnanecData.pozice : ""} onChange={handleZamestnanciInputChange}>
                                <option value="">-- Vyber pozici --</option>
                                {pozice.map((pozice) => (
                                  <option key={pozice.ID_pozice} value={pozice.nazev}>
                                    {pozice.nazev}
                                  </option>
                                ))}
                              </select>
                            </th>

                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" name="mesto" value={isEditingZamestnanec ? SelectedZamestnanecData.mesto : ""} onChange={handleZamestnanciInputChange} />
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" name="email" value={isEditingZamestnanec ? SelectedZamestnanecData.email : ""} onChange={handleZamestnanciInputChange} />
                            </th>
                            <th scope="col">
                              <input className="adduser_input editadduser_input" type="text" name="telefon" value={isEditingZamestnanec ? SelectedZamestnanecData.telefon : ""} onChange={handleZamestnanciInputChange} />
                            </th>
                          </tr>
                        )}
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Jméno a přijmení</th>
                          <th scope="col">Pozice</th>
                          <th scope="col">Město</th>
                          <th scope="col">Email</th>
                          <th scope="col">Telefon</th>
                        </tr>
                      </thead>
                      <tbody>
                        {employees.map((employees) => (
                          <tr key={employees.ID_zamestnanec} onClick={() => handleZamestnanciTableRowClick(employees)}>
                            <th scope="row">{employees.ID_zamestnanec}</th>
                            <td>{employees.jmeno + " " + employees.prijmeni}</td>
                            <td>{employees.pozice}</td>
                            <td>{employees.mesto}</td>
                            <td>{employees.email}</td>
                            <td>{employees.telefon}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </section>
              </>
            )}
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
