import React, { useState, useEffect } from "react";
import { Routes, Route, BrowserRouter, useLocation } from "react-router-dom";
import "./App.css";

import Home from "./pages/home/home";

function App() {
  return <>{<Home />}</>;
}

export default App;
